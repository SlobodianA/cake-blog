<h1> Blog Articles</h1>
<?= $this->Html->link(' Add article', ['action' => 'add']) ?>

<table>
    <tr>
        <th>Id</th>
        <th>Title</th>
        <th>Date</th>
        <th>Category</th>
        <th>Action</th>
    </tr>

    <?php foreach ($articles as $article): ?>
    <tr>
        <td><?= $article->id ?></td>
        <td>
            <?= $this->Html->link($article->title, ['action' => 'view', $article->id]) ?>
        </td>
        <td>
            <?= $article->created->format(DATE_RFC850) ?>
        </td>
        <td><?= $article->category_id ?></td>
        <td>
            <?= $this->Html->link('Edit', ['action' => 'edit', $article->id]) ?>,
            <?= $this->Form->postLink(
                'Delete',
                ['action' => 'delete', $article->id],
                ['confirm' => 'Вы уверены?'])
            ?>
        </td>
    </tr>
    <?php endforeach; ?>
</table>